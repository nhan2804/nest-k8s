import { Test, TestingModule } from '@nestjs/testing';
import { K8sApiController } from './k8s-api.controller';
import { K8sApiService } from './k8s-api.service';

describe('K8sApiController', () => {
  let controller: K8sApiController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [K8sApiController],
      providers: [K8sApiService],
    }).compile();

    controller = module.get<K8sApiController>(K8sApiController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
