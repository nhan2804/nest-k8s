import { Test, TestingModule } from '@nestjs/testing';
import { K8sApiService } from './k8s-api.service';

describe('K8sApiService', () => {
  let service: K8sApiService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [K8sApiService],
    }).compile();

    service = module.get<K8sApiService>(K8sApiService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
