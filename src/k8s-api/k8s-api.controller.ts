import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
} from '@nestjs/common';
import { K8sApiService } from './k8s-api.service';
import { CreateK8sApiDto } from './dto/create-k8s-api.dto';
import { K8sService } from 'src/k8s/k8s.service';

@Controller('k8s-api')
export class K8sApiController {
  constructor(private readonly k8sService: K8sService) {}

  @Get('summary')
  async summary(@Query('namespace') namespace: string) {
    const nodes = (await this.k8sService.k8sApi.listNode())?.body;
    const pods = (
      namespace
        ? await this.k8sService.k8sApi.listNamespacedPod(namespace)
        : await this.k8sService.k8sApi.listPodForAllNamespaces()
    )?.body;
    const svc = (
      namespace
        ? await this.k8sService.k8sApi.listNamespacedService(namespace)
        : await this.k8sService.k8sApi.listServiceForAllNamespaces()
    )?.body;
    const ns = (await this.k8sService.k8sApi.listNamespace())?.body;
    const cm = (
      namespace
        ? await this.k8sService.k8sApi.listNamespacedConfigMap(namespace)
        : await this.k8sService.k8sApi.listConfigMapForAllNamespaces()
    )?.body;
    const sa = (
      namespace
        ? await this.k8sService.k8sApi.listNamespacedServiceAccount(namespace)
        : await this.k8sService.k8sApi.listServiceAccountForAllNamespaces()
    )?.body;
    return {
      nodes,
      pods,
      svc,
      ns,
      sa,
      cm,
    };
  }
  @Get('top')
  async top() {
    (BigInt.prototype as any).toJSON = function () {
      return this.toString();
    };
    const topNodes = await this.k8sService.k8s.topNodes(this.k8sService.k8sApi);
    return {
      pathDiscovery: this.k8sService.k8sApi.basePath,
      KUBERNETES_SERVICE_HOST: process.env.KUBERNETES_SERVICE_HOST,
      KUBERNETES_SERVICE_PORT_HTTPS: process.env.KUBERNETES_SERVICE_PORT_HTTPS,
    };
    return topNodes;
  }
}
