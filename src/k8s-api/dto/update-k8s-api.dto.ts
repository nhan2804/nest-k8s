import { PartialType } from '@nestjs/mapped-types';
import { CreateK8sApiDto } from './create-k8s-api.dto';

export class UpdateK8sApiDto extends PartialType(CreateK8sApiDto) {}
