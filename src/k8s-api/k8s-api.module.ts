import { Module } from '@nestjs/common';
import { K8sApiService } from './k8s-api.service';
import { K8sApiController } from './k8s-api.controller';
import { K8sModule } from 'src/k8s/k8s.module';

@Module({
  controllers: [K8sApiController],
  providers: [K8sApiService],
  imports: [K8sModule.register()],
  //k
})
export class K8sApiModule {}
