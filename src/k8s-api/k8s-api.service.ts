import { Injectable } from '@nestjs/common';
import { CreateK8sApiDto } from './dto/create-k8s-api.dto';
import { UpdateK8sApiDto } from './dto/update-k8s-api.dto';

@Injectable()
export class K8sApiService {
  create(createK8sApiDto: CreateK8sApiDto) {
    return 'This action adds a new k8sApi';
  }

  findAll() {
    return `This action returns all k8sApi`;
  }

  findOne(id: number) {
    return `This action returns a #${id} k8sApi`;
  }

  update(id: number, updateK8sApiDto: UpdateK8sApiDto) {
    return `This action updates a #${id} k8sApi`;
  }

  remove(id: number) {
    return `This action removes a #${id} k8sApi`;
  }
}
