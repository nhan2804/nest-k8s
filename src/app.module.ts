import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { StoreModule } from './store/store.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { CacheModule } from '@nestjs/cache-manager';
import { TodoModule } from './todo/todo.module';
import { K8sModule } from './k8s/k8s.module';
import { K8sApiModule } from './k8s-api/k8s-api.module';
import * as redisStore from 'cache-manager-redis-store';
@Module({
  imports: [
    StoreModule,
    ConfigModule.forRoot({
      ignoreEnvFile: true,
      isGlobal: true,
    }),
    {
      ...CacheModule.registerAsync({
        imports: [ConfigModule],
        inject: [ConfigService],
        useFactory: (configService: ConfigService) => ({
          //@ts-ignore
          store: redisStore,
          host: configService.get('REDIS_HOST') || 'localhost',
          port: configService.get('REDIS_PORT') || 6379,
          isGlobal: true,
          password: 'Nnn2804..!',
        }),
      }),
      global: true,
    },
    TodoModule,
    K8sModule,
    K8sApiModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
