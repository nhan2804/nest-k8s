import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Inject,
  UseInterceptors,
} from '@nestjs/common';
import { TodoService } from './todo.service';
import { CreateTodoDto } from './dto/create-todo.dto';
import { UpdateTodoDto } from './dto/update-todo.dto';
import { Cache } from 'cache-manager';
import {
  CACHE_MANAGER,
  CacheInterceptor,
  CacheTTL,
  CacheKey,
} from '@nestjs/cache-manager';
import { catchError, firstValueFrom } from 'rxjs';
import { HttpService } from '@nestjs/axios';
import { AxiosError } from 'axios';
interface ITodo {
  userId: string;
  id: string;
  title: string;
  completed: boolean;
}
@Controller('todos')
export class TodoController {
  constructor(
    private readonly todoService: TodoService,
    @Inject(CACHE_MANAGER) private cacheManager: Cache,
    private readonly httpService: HttpService,
  ) {}

  @Post()
  create(@Body() createTodoDto: CreateTodoDto) {
    return this.todoService.create(createTodoDto);
  }
  @CacheKey('custom_key')
  @CacheTTL(10)
  @UseInterceptors(CacheInterceptor)
  @Get()
  async findAll() {
    const { data } = await firstValueFrom(
      this.httpService
        .get<ITodo[]>('https://jsonplaceholder.typicode.com/todos')
        .pipe(
          catchError((error: AxiosError) => {
            throw 'An error happened!';
          }),
        ),
    );
    const rs = await this.cacheManager.set('nhan', 'Hello', {
      ttl: 10,
    });
    return {
      data,
      redisInfo: rs,
      metadata: {
        cached: true,
      },
    };
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.todoService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateTodoDto: UpdateTodoDto) {
    return this.todoService.update(+id, updateTodoDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.todoService.remove(+id);
  }
}
