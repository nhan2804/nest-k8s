import { Module } from '@nestjs/common';
import { TodoService } from './todo.service';
import { TodoController } from './todo.controller';
import { HttpModule } from '@nestjs/axios/dist';
@Module({
  controllers: [TodoController],
  providers: [TodoService],
  imports: [HttpModule],
})
export class TodoModule {}
