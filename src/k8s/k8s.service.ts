import { Injectable } from '@nestjs/common';
import * as k8s from '@kubernetes/client-node';
@Injectable()
export class K8sService {
  readonly k8sApi: k8s.CoreV1Api;
  readonly k8s: any;
  constructor() {
    const kc = new k8s.KubeConfig();
    kc.loadFromDefault();
    this.k8s = k8s;
    this.k8sApi = kc.makeApiClient(k8s.CoreV1Api);
  }
}
