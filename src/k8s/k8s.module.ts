import { DynamicModule, Module } from '@nestjs/common';
import { K8sService } from './k8s.service';
@Module({})
export class K8sModule {
  static register(): DynamicModule {
    return {
      module: K8sModule,
      providers: [K8sService],
      exports: [K8sService],
    };
  }
}
