import { PartialType } from '@nestjs/mapped-types';
import { CreateK8Dto } from './create-k8.dto';

export class UpdateK8Dto extends PartialType(CreateK8Dto) {}
