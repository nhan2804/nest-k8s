import { Injectable } from '@nestjs/common';
import { CreateStoreDto } from './dto/create-store.dto';
import { UpdateStoreDto } from './dto/update-store.dto';
import { ConfigModule ,ConfigService} from '@nestjs/config';
@Injectable()
export class StoreService {
  constructor(private configService: ConfigService) {}
  create(createStoreDto: CreateStoreDto) {
    return 'This action adds a new store ';
  }

  findAll() {
    return {
      appName:this.configService.get("APP_NAME") || "NO_VAR",
      nodeName:this.configService.get("MY_NODE_NAME"),
      podName:this.configService.get("MY_POD_NAME"),
      podNameSpace:this.configService.get("MY_POD_NAMESPACE"),
      podIp:this.configService.get("MY_POD_IP"), 
      podServiceAccount:this.configService.get("MY_POD_SERVICE_ACCOUNT"),
      deployedBy:"Nguyễn Ngọc Nhẫn",
      version:"2.2",
      message:"Deploy using Gitlab CICD to K8S using Helm chart and ArgoCD for CD"
    }
  }

  findOne(id: number) {
    return `This action returns a #${id} store`;
  }

  update(id: number, updateStoreDto: UpdateStoreDto) {
    return `This action updates a #${id} store`;
  }

  remove(id: number) {
    return `This action removes a #${id} store`;
  }
}
